# rustdown

My crappy non-standards compliant markdown to HTML converter
that nobody should ever use for production.

This started with me venting my exam stress while writing some code in Rust.

## Usage

```bash
git clone https://gitlab.com/japorized/rustdown.git
cd rustdown
cargo run ./test/basic.md
```

---

## Checklist of tags

* ~~Headings~~
* ~~Links (with optional html options)~~ (e.g. 
`[link](https://example.com){target="_blank"} -> <a href="https://example.com" target="_blank">link</a>`)
* ~~Text Formatting~~ Also supports [mark](https://www.w3schools.com/tags/tag_mark.asp)
* ~~Blockquotes~~
* ~~ol, ul~~ (not thoroughly tested)
* code tag (partially done, but does not recognize syntax, for now)
* (optional) ~~keeping html comments~~
* ~~hr~~
* tables

---

## Other notes

* Performance is in the dumpsters before optimization by Rust itself
* I can never remember John Gruber's markdown syntax for text formatting :( Is it `_italic_` or `*italic*`, or is the latter a `*bold*`, or what about `**bold**`?

---

## (Perhaps) Non-standard syntax

* `**bold**` -> `<b>bold</b>`
* `~italic~` -> `<i>italic</i>`
* `__underline__` -> `<u>underline</u>`
* `--strikethrough--` -> `<s>strikethrough</s>`
* `|mark|` -> `<mark>mark</mark>` (will probably change cause I forgot about tables)
* `[link](https://example.com){target="_blank"}` -> `<a href="https://example.com" target="_blank">link</a>` (hopefully add similar support for almost all other directives)
