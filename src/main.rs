#[macro_use]
extern crate clap;
extern crate regex;

use std::io::{BufRead, BufReader, Result};
use std::fs::File;
use std::collections::HashMap;
use clap::App;
use lazy_static::lazy_static;
use regex::Regex;

lazy_static! {
    static ref RE_OL_MATCHER: Regex = Regex::new(r"^[0-9]+\.").unwrap();
    static ref RE_CODE_MATCHER: Regex = Regex::new(r"^```").unwrap();
    static ref RE_LINK_MATCHER: Regex = Regex::new(r"\[.*?\]\(.*?\)").unwrap();
    static ref RE_INLINECODE_MATCHER: Regex = Regex::new(r"`.*?`").unwrap();

    static ref RE_LINK_CATCHER: Regex = Regex::new(r"\[(?P<t>.*?)\]\((?P<u>.*?)\)(\{(?P<o>.*?)\})?").unwrap();
    static ref RE_ITALICS_CATCHER: Regex = Regex::new(r"~(?P<c>.+?)~").unwrap();
    static ref RE_BOLDS_CATCHER: Regex = Regex::new(r"\*\*(?P<c>.+?)\*\*").unwrap();
    static ref RE_STRIKES_CATCHER: Regex = Regex::new(r"\-\-(?P<c>.+?)\-\-").unwrap();
    static ref RE_UNDERLINES_ATCHER: Regex = Regex::new(r"__(?P<c>.+?)__").unwrap();
    static ref RE_MARKS_CATCHER: Regex = Regex::new(r"\|(?P<c>.+?)\|").unwrap();
    static ref RE_UL_LIS_CATCHER: Regex = Regex::new(r"^\*\s?(?P<c>.*)").unwrap();
    static ref RE_OL_LIS_CATCHER: Regex = Regex::new(r"^[0-9]+\.\s?(?P<c>.*)").unwrap();
    static ref RE_INLINECODES_CATCHER: Regex = Regex::new(r"`(?P<c>.*?)`").unwrap();
}

fn main() -> Result<()> {
    let yaml = load_yaml!("cli.yml");
    let argmatches = App::from_yaml(yaml).get_matches();

    if let Some(input) = argmatches.value_of("INPUT") {
        let lines = file_to_vec(input.to_string())?;
        // modes
        // 0 - no mode
        // 1 - p mode
        // 2 - ul mode
        // 3 - ol mode
        // 4 - blockquote mode
        // 5 - code mode (sort of like escape mode)
        let mut modes = HashMap::new();
        modes.insert(0, ("".to_string(), "".to_string()));
        modes.insert(1, ("<p>".to_string(), "</p>".to_string()));
        modes.insert(2, ("<ul>".to_string(), "</ul>".to_string()));
        modes.insert(3, ("<ol>".to_string(), "</ol>".to_string()));
        modes.insert(4, ("<blockquote>".to_string(), "</blockquote>".to_string()));
        modes.insert(5, ("<pre><code>".to_string(), "</code></pre>".to_string()));
        let mut mode = 0;

        for line in lines {
            let mut l: String = line;

            // if it's an empty line, skip, for now
            if &l == "" {
                match mode {
                    1 | 2 | 3 | 4 | 5 => {
                        println!("{}", modes[&{mode}].1);
                        mode = 0;
                    },
                    _ => (),
                }
                continue;
            }

            if l.chars().count() > 3 {
                if &l[..4] == "<!--" {
                    if argmatches.is_present("keepcomments") {
                        println!("{}", l);
                    }
                    continue;
                }
            }

            if &l[..3] == "---" {
                println!("{}", modes[&{mode}].0);
                mode = 0;
                println!("{}", "<hr />");
                continue;
            }

            l = convert_italic(&l);
            l = convert_underline(&l);
            l = convert_strikethrough(&l);
            l = convert_bold(&l);
            l = convert_marks(&l);

            // convert all links to html
            if has_link(&l) {
                l = convert_link(&l);
            }

            if has_inlinecode(&l) {
                l = convert_inlinecode(&l);
            }

            // first, check if the line is a heading
            // convert to heading if it is
            if is_heading_line(&l) {
                l = heading_line(&l);
            } else if is_ul_line(&l) {
                if mode != 2 { 
                    mode = 2;
                    println!("{}", modes[&{mode}].0);
                }
                l = format!("{}", convert_ulli(&l));
            } else if is_ol_line(&l) {
                if mode != 3 { 
                    mode = 3;
                    println!("{}", modes[&{mode}].0);
                }
                l = format!("{}", convert_olli(&l));
            } else if is_blockquote_line(&l) {
                if mode != 4 {
                    mode = 4;
                    println!("{}", modes[&{mode}].0);
                }
                l = format!("{}", &l[2..]);
            } else if is_code_line(&l) {
                if mode != 5 {
                    mode = 5;
                    println!("{}", modes[&{mode}].0);
                } else {
                    println!("{}", modes[&{mode}].1);
                    mode = 0;
                }
                continue;
            } else if mode != 5 {
                if mode != 1 {
                    mode = 1;
                    l = format!("{}\n{}", modes[&{mode}].0, l);
                }
            }

            println!("{}", l);
        }

        println!("{}", modes[&{mode}].1);
    }

    Ok(())
}

fn is_heading_line(_fileline: &String) -> bool {
    match &_fileline[..1] {
        "#" => true,
        _ => false
    }
}

fn is_blockquote_line(_fileline: &String) -> bool {
    match &_fileline[..1] {
        ">" => true,
        _ => false
    }
}

fn is_ul_line(_fileline: &String) -> bool {
    match &_fileline[..2] {
        "* " => true,
        _ => false
    }
}

fn is_ol_line(_fileline: &String) -> bool {
    RE_OL_MATCHER.is_match(&_fileline)
}

fn is_code_line(_fileline: &String) -> bool {
    RE_CODE_MATCHER.is_match(&_fileline)
}

fn has_link(_fileline: &String) -> bool {
    RE_LINK_MATCHER.is_match(&_fileline)
}

fn has_inlinecode(_fileline: &String) -> bool {
    RE_INLINECODE_MATCHER.is_match(&_fileline) && ! is_code_line(&_fileline)
}

fn convert_link(_fileline: &String) -> String {
    RE_LINK_CATCHER.replace_all(&_fileline, "<a href=\"$u\" $o>$t</a>").to_string()
}

fn convert_italic(_fileline: &String) -> String {
    RE_ITALICS_CATCHER.replace_all(&_fileline, "<i>$c</i>").to_string()
}

fn convert_bold(_fileline: &String) -> String {
    RE_BOLDS_CATCHER.replace_all(&_fileline, "<b>$c</b>").to_string()
}

fn convert_strikethrough(_fileline: &String) -> String {
    RE_STRIKES_CATCHER.replace_all(&_fileline, "<s>$c</s>").to_string()
}

fn convert_underline(_fileline: &String) -> String {
    RE_UNDERLINES_ATCHER.replace_all(&_fileline, "<u>$c</u>").to_string()
}

fn convert_marks(_fileline: &String) -> String {
    RE_MARKS_CATCHER.replace_all(&_fileline, "<mark>$c</mark>").to_string()
}

fn convert_ulli(_fileline: &String) -> String {
    let grp = RE_UL_LIS_CATCHER.captures(&_fileline).unwrap();
    format!("<li>{}</li>", &grp[1])
}

fn convert_olli(_fileline: &String) -> String {
    let grp = RE_OL_LIS_CATCHER.captures(&_fileline).unwrap();
    format!("<li>{}</li>", &grp[1])
}

fn convert_inlinecode(_fileline: &String) -> String {
    RE_INLINECODES_CATCHER.replace_all(&_fileline, "<code>$c</code>").to_string()
}

fn heading_line(line: &String) -> String {
    let mut heading_level = 0;
    for (i, c) in line.chars().enumerate() {
        if c == '#' {
            heading_level = i + 1;
        }

        if i > 5 {
            break;
        }
    }
    // check if there's a space after the last # char, and deal with that accordingly
    let mut headingline: String = line[heading_level..line.chars().count()].to_string();
    if headingline.chars().next().unwrap() == ' ' {
        headingline = headingline[1..].to_string();
    }
    match heading_level {
        1 => headingline = format!("<h1>{}</h1>", headingline),
        2 => headingline = format!("<h2>{}</h2>", headingline),
        3 => headingline = format!("<h3>{}</h3>", headingline),
        4 => headingline = format!("<h4>{}</h4>", headingline),
        5 => headingline = format!("<h5>{}</h5>", headingline),
        6 => headingline = format!("<h6>{}</h6>", headingline),
        _ => (),
    }
    
    headingline
}

fn file_to_vec(filename: String) -> Result<Vec<String>> {
    let file_in = File::open(filename)?;
    let file_reader = BufReader::new(file_in);
    Ok(file_reader.lines().filter_map(Result::ok).collect())
}
