<!--                 _      _                     -->
<!--                | |    | |                    -->
<!--  _ __ _   _ ___| |_ __| | _____      ___ __  -->
<!-- | '__| | | / __| __/ _` |/ _ \ \ /\ / / '_ \ -->
<!-- | |  | |_| \__ \ || (_| | (_) \ V  V /| | | |-->
<!-- |_|   \__,_|___/\__\__,_|\___/ \_/\_/ |_| |_|-->
<!--                                              -->

# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6 [Link up](https://japorized.ink){target="_blank"} [Link 2](https://duckduckgo.com)

---

[Link](https://japorized.ink)

---

\* Bullet point 1
* Bullet point 2
* Bullet point 3
* Bullet point 4

1. Ordered item 1
2. Ordered item 2
3. Ordered item 3
4. Ordered item 4
5. Ordered item 5

1. Another ordered item
2. Another ordered item

rustdown should cover this entire
portion up to about here in a p tag.

> Quote
> Quote line 2

> Quote single line

<!-- This is a comment -->

*bold*

**bold**

~italic~

--strikethrough--

__Underline__

`inline code`

|marks|

```rust
  fn main() {
      println!("{}", "Hello world!");
  }
```

This is a more complicated line with **bold**, ~italics~, --strikethroughs--, `inline code` and |marks| |1| and |2|,
and then more **bolds** and ~italics~ and --strikethroughs-- and `inline code` and |marks|.
